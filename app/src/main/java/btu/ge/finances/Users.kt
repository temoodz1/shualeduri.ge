package btu.ge.finances

import com.google.gson.annotations.SerializedName


class Users {
    var userId = ""
    var userName = ""
    var userLast = ""
    var userBalance = ""
    var userProfileUrl = ""
}