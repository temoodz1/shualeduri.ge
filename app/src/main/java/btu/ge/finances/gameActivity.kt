package btu.ge.finances

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_game.*

class gameActivity : AppCompatActivity() {
    private var firstPlayer = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        init()
    }
    private fun init() {
        buttonR.setOnClickListener() {
            restgame()
        }
        button00.setOnClickListener() {
            checkPlayer(button00)
        }
        button01.setOnClickListener() {
            checkPlayer(button01)
        }
        button02.setOnClickListener() {
            checkPlayer(button02)
        }


        button10.setOnClickListener() {
            checkPlayer(button10)
        }
        button11.setOnClickListener() {
            checkPlayer(button11)
        }
        button12.setOnClickListener() {
            checkPlayer(button12)
        }
        button20.setOnClickListener() {
            checkPlayer(button20)
        }
        button21.setOnClickListener() {
            checkPlayer(button21)
        }
        button22.setOnClickListener() {
            checkPlayer(button22)
        }
    }


    private fun checkPlayer(button: Button) {
        if (button.text.isEmpty()) {
            if (firstPlayer) {
                button.text = "x"

                firstPlayer = false
            } else {
                button.text = "0"

                firstPlayer = true
            }

        }

    }

    private fun checkWinner() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    private fun restgame() {
        button00.text = ""
        button01.text = ""
        button02.text = ""
        button10.text = ""
        button11.text = ""
        button12.text = ""
        button20.text = ""
        button21.text = ""
        button22.text = ""
    }


}





