package btu.ge.finances

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.google.gson.internal.GsonBuildConfig
import kotlinx.android.synthetic.main.splash_screen.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)
        init()
    }


    private fun init() {


//        main_logo.startAnimation(
//            AnimationUtils.loadAnimation(
//                baseContext,
//                R.anim.left
//            )
//        )

        main_text.startAnimation(
            AnimationUtils.loadAnimation(
                baseContext,
                R.anim.right
            )
        )

        val handler = Handler().postDelayed({
            startActivity(Intent(baseContext, LoginActivity::class.java))
        }, 5000)


    }


}
