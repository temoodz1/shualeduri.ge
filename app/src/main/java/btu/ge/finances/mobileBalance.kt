package btu.ge.finances

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_mobile_balance.*


class mobileBalance : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mobile_balance)
        init()
    }


    private lateinit var database: DatabaseReference


    private fun init() {

        database = FirebaseDatabase.getInstance().getReference("user")


        payButon.setOnClickListener {
            if (mobileNum.text.isEmpty() || TransactionMoney.text.isEmpty()) {
                Toast.makeText(this, " შეავსეთ ყველა ველი", Toast.LENGTH_LONG)
                    .show()

            } else if (mobileNum.text.length != 9) {
                Toast.makeText(this, " შეიყვანეთ მობილური ნომრის სწორი ფორმატი", Toast.LENGTH_LONG)
                    .show()
            } else {
                val intent = intent
                val current_balance = intent!!.getStringExtra("user_balance")
                Toast.makeText(this, current_balance, Toast.LENGTH_SHORT).show()
                finish()
                editBalance(current_balance)
            }
        }
    }

    private fun editBalance(user_balance: String) {
        val money = (user_balance.toString().toInt() - TransactionMoney.text.toString().toInt())
        database.child("balance").setValue(money)
    }



}