package btu.ge.finances

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class LoginActivity : AppCompatActivity() {


    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {


        val animate = AnimationUtils.loadAnimation(this, R.anim.login)
        loginBody.startAnimation(animate)


        val sharePreferences = getSharedPreferences("user_data", Context.MODE_PRIVATE)
        emailEditText.setText(sharePreferences.getString("email", ""))
        passwordEditText.setText(sharePreferences.getString("password", ""))

        registrationButton.setOnClickListener {
            startActivity(Intent(this, registrationActivity::class.java))
        }
    }


    fun login(view: View) {

        auth = FirebaseAuth.getInstance()


        if (emailEditText.text.isNotEmpty() && passwordEditText.text.isNotEmpty()) {

            auth.signInWithEmailAndPassword(
                emailEditText.text.toString(),
                passwordEditText.text.toString()
            )
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {

                        val sharedPreferences =
                            getSharedPreferences("user_data", Context.MODE_PRIVATE)
                        val edit = sharedPreferences.edit()
                        edit.putString("email", emailEditText.text.toString())
                        edit.putString("password", passwordEditText.text.toString())
                        edit.commit()


                        val intent = Intent(this, DashboardActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                    } else {
                        Toast.makeText(
                            this,
                            "მომხმარებლის სახელი და პაროლი არასწორია",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                }

        } else {
            Toast.makeText(this, "შეავსეთ ყველა მოცემული ველი", Toast.LENGTH_SHORT).show()
        }

    }


}
