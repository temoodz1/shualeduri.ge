package btu.ge.finances

class UserInfoModel {
    var id : Int = 0
    var email : String = ""
    var name : String = ""
    var lastname : String = ""
    var balance : Int = 0
    var scores: Int = 0

}