package btu.ge.finances

import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.PartMap
import retrofit2.http.Path

interface ApiService {

    companion object {
        val BASE_URL = "https://api.cloud.com.ge/finance/api/"
    }

    @GET("myInfo")
    fun postRequest(@Path("path") path: String, @PartMap parameters: Map<String, @JvmSuppressWildcards RequestBody>): Call<String>

}