package btu.ge.finances

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.internal.GsonBuildConfig
import kotlinx.android.synthetic.main.activity_dashboard.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class DashboardActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        init()
    }

    private fun init() {
//        val database = FirebaseDatabase.getInstance()
        val myRef = FirebaseDatabase.getInstance().getReference("user")

        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.getValue(UserInfoModel::class.java)
                userBalanceTextView.setText(value?.balance.toString() + " ₾ ")
                userScoreTextView.setText(value?.scores.toString())


                balanceButton.setOnClickListener {
                    //            val intent
                    val current_balance = value?.balance
                    val intent = Intent(baseContext, mobileBalance::class.java)
                    intent.putExtra("user_balance", current_balance)
                    startActivity(intent)
                    finish()
                }


            }

            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(this@DashboardActivity, "error", Toast.LENGTH_SHORT).show()
            }
        })



        gameButton.setOnClickListener {
            startActivity(Intent(this, gameActivity::class.java))
        }

    }

}
