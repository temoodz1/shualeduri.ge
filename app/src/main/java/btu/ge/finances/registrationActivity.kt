package btu.ge.finances

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_registration.*

class registrationActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        auth = FirebaseAuth.getInstance()
        init()


        LoginButton.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

    private fun init() {

        R_registrationButton.setOnClickListener {
            if (R_emailEditText.text.isEmpty() && R_passwordEditText.text.isEmpty()) {
                Toast.makeText(this, "Please fill all fields", Toast.LENGTH_LONG).show()
            } else if (R_emailEditText.text.isEmpty() && R_passwordEditText.text.isNotEmpty()) {
                Toast.makeText(this, "Please enter your email", Toast.LENGTH_LONG).show()
            } else if (R_emailEditText.text.isNotEmpty() && R_passwordEditText.text.isEmpty()) {
                Toast.makeText(this, "Please enter your Password", Toast.LENGTH_LONG).show()
            } else if (R_passwordEditText.text.toString() != R_passwordConfirmEditText.text.toString()) {
                Toast.makeText(this, "password did not match", Toast.LENGTH_SHORT).show()
            } else {
                auth.createUserWithEmailAndPassword(
                    R_emailEditText.text.toString(),
                    R_passwordEditText.text.toString()
                )
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(
                                this,
                                "Registration was Successful. please return in to login page",
                                Toast.LENGTH_LONG
                            ).show()

                        } else {

                            Toast.makeText(
                                baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT
                            ).show()


                        }
                    }
            }


        }
    }
}
